/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.sql.*;
/**
 *
 * @author elpep
 */
public abstract class dbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registros;
    
    private String usuario,contraseña,baseDatos,drive,url;

    public dbManejador(Connection conexion, PreparedStatement splConsulta, ResultSet registros, String usuario, String contraseña, String baseDatos, String drive, String url) {
        this.conexion = conexion;
        this.sqlConsulta = splConsulta;
        this.registros = registros;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.baseDatos = baseDatos;
        this.drive = drive;
        this.url = url;
    }

    public dbManejador() {
        this.baseDatos = "Sistemas";
        this.usuario = "root";
        this.contraseña = "contraseña";
        this.drive = "com.mysql.oj.jdbc.Driver";
        this.url = "jdbc:mysql://locahost:3306" + this.baseDatos;
        this.EsDrive();
    }

    public boolean EsDrive(){
        boolean kaft = false;
        
        try{
            Class.forName(drive);
            kaft=true;
        }catch(ClassNotFoundException e){
            
            System.out.println("surgio un error " + e.getMessage());
            System.exit(-1);
        }
        return kaft;
    }
    
    public boolean conectar(){
    boolean kaft = false;
    try {
        this.setConexion(DriverManager.getConnection(url, usuario, contraseña));
        kaft=true;
    } 
    catch(SQLException e){
        System.out.println("Surgio un error " + e.getMessage());
    }
    return kaft;
    }
    
    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistros() {
        return registros;
    }

    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getBaseDatos() {
        return baseDatos;
    }

    public void setBaseDatos(String baseDatos) {
        this.baseDatos = baseDatos;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    
}
